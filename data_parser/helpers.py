import requests

from requests.exceptions import Timeout

from .const import (URL, TIMEOUT, SPLIT_TOKEN,
                    FLOATABLES, STATUS_CODE_SUCCESS)


def api_data():
    """API for Client Data.

    Returns:
        requests.response or None: Client API data.

    Raises:
        requests.exceptions.Timeout

    """
    try:
        return requests.get(URL, timeout=TIMEOUT)
    except Timeout:
        return None


def constant_data():
    """Alternative way of Client Data Access.

    Raises:
        NotImplementedError

    """
    raise NotImplementedError("[ ERROR] yet to build!")


def split_data(data, split_token=SPLIT_TOKEN):
    """Splits the API Data.

    Args:
        data (str): API Data.
        split_token (str, optional): Split String Token.

    Returns:
        list: List of splitted data across the split token.

    Raises:
        ValueError or TypeError

    """
    if not data:
        raise ValueError("[ ERROR] data cannot be empty!")

    if not split_token:
        raise ValueError("[ ERROR] split_token cannot be empty!")

    if not isinstance(data, str):
        raise TypeError("[ ERROR] data is not of <type: str>!")

    return data.split(split_token)


def normalize_data(data):
    """Normalizes the data with appropriate data fields.

    Args:
        data (list): client data with latitude, longitude, user_id, and name.

    Returns:
        list: normalized client data

    Notes:
        1. Parsing the dataset using 'eval' since the dataset is not a valid JSON.
        2. Avoiding List Comprehension to make the code more efficient and readable.

    """
    norm_data = []
    for client in map(eval, data or []):
        client_data = {}
        for k, v in client.items():
            if k in FLOATABLES:
                client_data[k] = float(v)
            else:
                client_data[k] = v
        norm_data += [client_data]
    return norm_data


def clean_api_data(data_object):
    """Preprocesses the API Data.

    Args:
        data_object (requests.response): API Response.

    Returns:
        list: Client API data.

    Raises:
        ValueError or TypeError

    """
    if not data_object:
        raise ValueError("[ ERROR] api response object empty!")

    if not data_object.status_code in STATUS_CODE_SUCCESS:
        raise ValueError("[ ERROR] api returned HTTP Status: {}".format(data_object.status_code))
    return normalize_data(split_data(data_object.text))

