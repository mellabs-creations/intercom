import unittest
import requests

from unittest.mock import Mock, patch
from requests.exceptions import Timeout

from .const import (STATUS_CODE_404, API_TEXT, PARSED_API_TEXT,
                    SPLITTED_API_TEXT, STATUS_CODE_200)
from ..core import Parser, DataHandler
from ..helpers import (constant_data, split_data, normalize_data,
                       clean_api_data, api_data)


class TestParser(unittest.TestCase):
    def test_perform__Exception(self):
        with patch("data_parser.core.Parser._Parser__is_data_obj_present") as dop:
            dop.side_effect = Exception
            self.assertRaises(Exception, Parser().perform, None, None)
            self.assertRaises(Exception, Parser().perform, None, "api")
            self.assertRaises(Exception, Parser().perform, None, "constant")

    def test_perform__TypeError(self):
        success_dobj = Mock(status_code=200, text=API_TEXT)
        failure_dobj = Mock(status_code=404, text=None)
        with patch("data_parser.core.Parser._Parser__is_technique_present") as tp:
            tp.side_effect = TypeError
            self.assertRaises(TypeError, Parser().perform, success_dobj, None)
            self.assertRaises(TypeError, Parser().perform, failure_dobj, None)

    def test_get_cleaned_data_using__KeyError(self):
        success_dobj = Mock(status_code=200, text=API_TEXT)
        failure_dobj = Mock(status_code=404, text=None)
        with patch("data_parser.core.Parser") as p:
            p.get_cleaned_data_using["constant"].side_effect = KeyError
            self.assertRaises(NotImplementedError, Parser().perform, success_dobj, "constant")
            self.assertRaises(NotImplementedError, Parser().perform, failure_dobj, "constant")

    def test_perform__ValueError(self):
        failure_dobj = Mock(status_code=404, text=None)
        self.assertRaises(ValueError, Parser().perform, failure_dobj, "api")

    def test_perform__NotImplementedError(self):
        success_dobj = Mock(status_code=200, text=API_TEXT)
        failure_dobj = Mock(status_code=404, text=None)
        with patch("data_parser.core.Parser.perform", side_effect=NotImplementedError) as p:
            self.assertRaises(NotImplementedError, Parser().perform, success_dobj, "constant")
            self.assertRaises(NotImplementedError, Parser().perform, failure_dobj, "constant")


class TestHelpers(unittest.TestCase):
    def test_api_data__Timeout(self):
        with patch("data_parser.helpers.requests") as r:
            r.get.side_effect = Timeout
            self.assertEqual(api_data(), None)

    def test_split_data__ValueError(self):
        self.assertRaises(ValueError, split_data, API_TEXT, None)
        self.assertRaises(ValueError, split_data, API_TEXT, "")
        self.assertRaises(ValueError, split_data, None)
        self.assertRaises(ValueError, split_data, "")
        self.assertRaises(ValueError, split_data, None, None)
        self.assertRaises(ValueError, split_data, "", "")

    def test_split_data__TypeError(self):
        self.assertRaises(TypeError, split_data, [""])
        self.assertRaises(TypeError, split_data, [""], "\n")

    def test_normalize_data__EmptyData(self):
        self.assertEqual(normalize_data(None), [])
        self.assertEqual(normalize_data([]), [])
        self.assertEqual(normalize_data({}), [])
        self.assertEqual(normalize_data(""), [])

    def test_clean_api_data__ValueError(self):
        failure_dobj = Mock(status_code=404, text=None)
        self.assertRaises(ValueError, clean_api_data, None)
        self.assertRaises(ValueError, clean_api_data, failure_dobj)

    def test_clean_api_data__Map(self):
        with patch("data_parser.core.map", return_value=SPLITTED_API_TEXT):
            self.assertEqual(normalize_data(SPLITTED_API_TEXT), PARSED_API_TEXT)


class TestHandlers(unittest.TestCase):
    def test_perform__TypeError(self):
        with patch("data_parser.core.DataHandler._DataHandler__is_technique_present") as tp:
            tp.side_effect = TypeError
            self.assertRaises(TypeError, DataHandler()._DataHandler__is_technique_present, None)

    def test_get_data_using__KeyError(self):
        with patch("data_parser.core.DataHandler") as dh:
            dh.get_data_using["random"].side_effect = KeyError
            self.assertRaises(NotImplementedError, DataHandler().perform, "random")

    def test_get_data_using__NotImplementedError(self):
        with patch("data_parser.core.DataHandler") as dh:
            dh.get_data_using["constant"].side_effect = NotImplementedError
            self.assertRaises(NotImplementedError, DataHandler().perform, "constant")

    def test_perform__NotImplementedError(self):
        with patch("data_parser.core.DataHandler.perform", side_effect=NotImplementedError) as dh:
            self.assertRaises(NotImplementedError, DataHandler().perform, "constant")
            self.assertRaises(NotImplementedError, DataHandler().perform, "random")


if __name__ == "__main__":
    unittest.main()
