import unittest

from unittest.mock import Mock, patch

from .const import (API_TEXT, STATUS_CODE_200, PARSED_API_TEXT,
                    SPLITTED_API_TEXT, SPLIT_TOKEN)
from ..core import DataHandler, Parser
from ..helpers import (api_data, constant_data, split_data,
                       normalize_data, clean_api_data)


class TestDataHandler(unittest.TestCase):
    def test__is_technique_present__success(self):
        self.assertRaises(TypeError, DataHandler()._DataHandler__is_technique_present, None)

    def test_get_data_using__success(self):
        success_dobj = Mock(status_code=200, text=API_TEXT)
        with patch("data_parser.core.DataHandler") as dh:
            dh.get_data_using["api"].return_value = success_dobj
            dh_test = DataHandler().perform("api")
            self.assertEqual(dh_test.status_code, STATUS_CODE_200)
            self.assertEqual(dh_test.text, API_TEXT)

    def test_perform__success(self):
        success_ad = Mock(status_code=200, text=API_TEXT)
        with patch("data_parser.core.api_data", return_value=success_ad) as ad:
            dh_test = DataHandler().perform("api")
            self.assertEqual(dh_test.status_code, STATUS_CODE_200)
            self.assertEqual(dh_test.text, API_TEXT)


class TestParser(unittest.TestCase):
    def test__is_data_obj_present__success(self):
        self.assertRaises(Exception, Parser()._Parser__is_data_obj_present, None)

    def test__is_technique_present__success(self):
        self.assertRaises(TypeError, Parser()._Parser__is_technique_present, None)

    def test_get_clean_data_using__success(self):
        success_dobj = Mock(status_code=200, text=API_TEXT)
        with patch("data_parser.core.Parser") as p:
            p.get_clean_data_using["api"].return_value = PARSED_API_TEXT
            self.assertEqual(Parser().perform(success_dobj, "api"), PARSED_API_TEXT)

    def test_perform__success(self):
        success_dobj = Mock(status_code=200, text=API_TEXT)
        with patch("data_parser.core.clean_api_data", return_value=PARSED_API_TEXT) as cad:
            self.assertEqual(Parser().perform(success_dobj, "api"), PARSED_API_TEXT)


class TestHelpers(unittest.TestCase):
    def test_api_data__success(self):
        with patch("data_parser.helpers.requests") as r:
            r.get.return_value = Mock(status_code=200, text=API_TEXT)
            self.assertEqual(api_data(), r.get())

    def test_constant_data__success(self):
        self.assertRaises(NotImplementedError, constant_data)

    def test_split_data__success(self):
        self.assertEqual(split_data(API_TEXT), SPLITTED_API_TEXT)
        self.assertEqual(split_data(API_TEXT, SPLIT_TOKEN), SPLITTED_API_TEXT)

    def test_normalized_data__success(self):
        self.assertEqual(normalize_data(SPLITTED_API_TEXT), PARSED_API_TEXT)

    def test_clean_api_data__success(self):
        data_object = Mock(status_code=200, text=API_TEXT)
        self.assertEqual(clean_api_data(data_object), PARSED_API_TEXT)


if __name__ == "__main__":
    unittest.main()
