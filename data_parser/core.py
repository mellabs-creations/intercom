from .helpers import api_data, clean_api_data, constant_data


class DataHandler(object):
    def __init__(self, *args, **kwargs):
        """Data Handling Service based on API.

        Args:
            args (list): Function Arguments.
            kwargs (dict): Function Arguments as key-value pair.

        """
        self.get_data_using = {
            "api": api_data,
            "constant": constant_data
        }

    @staticmethod
    def __is_technique_present(x):
        """Checks if technique is present.

        Raises:
            TypeError

        """
        if not x:
            raise TypeError("[ ERROR] 'None' technique does not exist!")

    def perform(self, technique, *args, **kwargs):
        """Performs data handling functionality.

        Args:
            technique (str): Data handling service technique.
            args (list): Function Arguments.
            kwargs (kwargs): Function Arguments as key-value pair.

        Returns:
            requests.response or None: JSON decoded Client API Data.

        Raises:
            TypeError or NotImplementedError

        """
        self.__is_technique_present(technique)

        try:
            return self.get_data_using[technique]()
        except KeyError:
            raise NotImplementedError("[ ERROR] '{}' technique is not implemented!".format(technique))


class Parser(object):
    def __init__(self, *args, **kwargs):
        """Data Parsing Service

        Args:
            args (list): Function Arguments.
            kwargs (dict): Function Arguments as key-value pair.

        """
        self.get_cleaned_data_using = {
            "api": clean_api_data
        }

    @staticmethod
    def __is_data_obj_present(x):
        """Checks if data_object is present.

        Raises:
            Exception

        """
        if not x:
            raise Exception("[ ERROR] api failed to return any result!")

    @staticmethod
    def __is_technique_present(x):
        """Checks if technique is present.

        Raises:
            TypeError

        """
        if not x:
            raise TypeError("[ ERROR] 'None' technique does not exist!")

    def perform(self, data_object, technique, *args, **kwargs):
        """Performs data preprocessing for Client API Data.

        Args:
            data_object (requests.response): Client API Response Object.
            technique (str): Data parsing technique.
            args (list): Function Arguments.
            kwargs (dict): Function Arguments as key-value pair.

        Returns:
            list: Parsed Client API Data.

        Raises:
            Exception or TypeError or NotImplementedError

        """
        self.__is_data_obj_present(data_object)
        self.__is_technique_present(technique)

        try:
            return self.get_cleaned_data_using[technique](data_object)
        except KeyError:
            raise NotImplementedError("[ ERROR] '{}' technique is not implemented!".format(technique))

