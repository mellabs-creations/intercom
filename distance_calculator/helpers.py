import os.path
import json
import math


from .const import (LAT_LONG_KEYS, DUBLIN_HQ_LAT, DUBLIN_HQ_LONG,
                    EARTH_RADIUS, INVITATION_PROXIMITY, OUTPUT_FILE_PATH)


def convert_to_radians(data):
    """Converts the degrees to radians for all the values of latitude and longitude.

    Args:
        data (dict): Client API Data.

    Returns:
        dict: Client API Data.

    Raises:
        ValueError

    Notes:
        1. Inplace computation for each instance of Client API Data is not possible
        as Dictionary is a mutable data structure and which can cause TestSuite to fail.

    """
    if not data:
        raise ValueError("[ ERROR] data cannot be empty!")

    _data = {}
    for k, v in data.items():
        if k in LAT_LONG_KEYS:
            _data[k] = math.radians(v)
        else:
            _data[k] = v
    return _data


def get_A_B(data):
    """Returns Point A and Point B values of Latitude and Longitude.

    Args:
        data (dict): Client API Data.

    Returns:
        float, float, float, float: L1, T1, L2, T2.

    Raise:
        ValueError

    """
    if not data:
        raise ValueError("[ ERROR] data cannot be empty!")

    return (data["longitude"],
            data["latitude"],
            math.radians(DUBLIN_HQ_LONG),
            math.radians(DUBLIN_HQ_LAT))


def compute_d_rho(L1, T1,L2, T2):
    """Computes the central angle for the lat-long of client wrt Intercom Dublin HQ.

    Args:
        L1 (float): longitude of client.
        T1 (float): latitude of client.
        L2 (float): longitude of Intercom Dublin HQ.
        T2 (float): latitude of Intercom Dublin HQ.

    Returns:
        float: computed central angle.

    """
    if not all([L1, T1, L2, T2]):
        raise ValueError("[ ERROR] some parameters are missing!")

    return math.acos((math.sin(T1) * math.sin(T2)) +
                     (math.cos(T1) * math.cos(T2) * math.cos(L2 - L1)))


def compute_d(d_rho):
    """Computes the arc length of distance from Client to Intercom's Dublin HQ.

    Args:
        d_rho (float): Central Angle.

    Returns:
        float: Arc Length.

    """
    if not d_rho:
        raise ValueError("[ ERROR] D-rho is not computed!")

    return EARTH_RADIUS * d_rho


def get_invited_clients(data):
    """Returns the invited clients across INVITATION_PROXIMITY.

    Args:
        data (dict): Client Data with Distance Computed.

    Returns:
        list: Sorted list of Clients who should be invited to Intercom's Dublin HQ.

    Raises:
        ValueError

    Notes:
        1. Using Tim Sort to sort the results based on ascending order of "user_id".

    """
    if not isinstance(data, list):
        raise ValueError("[ ERROR] client data empty!")

    if data == []:
        return data

    clients = [
        {
            "user_id": client_data["user_id"],
            "name": client_data["name"]
        } for client_data in data
        if client_data["distance"] <= INVITATION_PROXIMITY
    ]
    clients.sort(key=lambda x: x["user_id"])
    return clients


def write_output_to_file(output):
    """Writes Output to a File.

    Args:
        output (str): Output File Path.

    """
    json_object = json.dumps(output, indent=4)
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(my_path, OUTPUT_FILE_PATH)
    with open(path, "w+") as f:
        f.write(json_object)
        f.close()

