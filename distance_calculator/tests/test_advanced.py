import unittest

from unittest.mock import mock_open, patch

from .const import (DATA_DEGREE, DATA_RADIAN,
                    DUBLIN_HQ_LAT_RADIAN, DUBLIN_HQ_LONG_RADIAN,
                    D_RHO, L1, T1, L2, T2, DISTANCE,
                    DISTANCE_CALCULATED_TEXT, INVITATION_LIST,
                    OUTPUT_FILE_CONTENT)
from ..core import GreatCircleDistance, InvitationGenerator
from ..helpers import (convert_to_radians, get_A_B, compute_d_rho,
                       compute_d, get_invited_clients, write_output_to_file)


class TestGreatCircleDistance(unittest.TestCase):
    @patch("distance_calculator.core.compute_d_rho", return_value=0.0)
    @patch("distance_calculator.core.get_A_B", return_value=(L1,T1,L2,T2))
    def test_compute_distance__compute_d__ValueError(self, gab, crd):
        data = [
            {
                'latitude': 0.0,
                'user_id': 69,
                'name': 'John Doe',
                'longitude': 0.0
            }
        ]
        self.assertRaises(ValueError, GreatCircleDistance().compute_distance, data)

    def test_compute_distance__compute_d_rho__ValueError(self):
        with patch("distance_calculator.core.get_A_B", return_value=(None, T1, L2, T2)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(None, T1, L2, None)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(None, T1, None, T2)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(None, T1, None, None)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(None, None, L2, T2)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(None, None, L2, None)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(None, None, None, T2)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(None, None, None, None)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(L1, T1, L2, None)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(L1, T1, None, T2)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(L1, T1, None, None)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(L1, None, L2, T2)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(L1, None, L2, None)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(L1, None, None, T2)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        with patch("distance_calculator.core.get_A_B", return_value=(L1, None, None, None)):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        data = [
            {
                'latitude': 0.0,
                'user_id': 69,
                'name': 'John Doe',
                'longitude': 0.0
            }
        ]
        self.assertRaises(ValueError, GreatCircleDistance().compute_distance, data)

    def test_compute_distance__get_A_B__ValueError(self):
        with patch("distance_calculator.core.get_A_B", side_effect=ValueError):
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
            self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])

    def test_compute_distance__ValueError(self):
        self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [None])
        self.assertRaises(ValueError, GreatCircleDistance().compute_distance, [""])
        data = [
            {
                'latitude': 0.0,
                'user_id': 69,
                'name': 'John Doe',
                'longitude': 0.0
            }
        ]
        self.assertRaises(ValueError, GreatCircleDistance().compute_distance, data)

    def test_calculate__convert_to_radians__ValueError(self):
        with patch("distance_calculator.core.convert_to_radians", side_effect=ValueError):
            self.assertRaises(ValueError, GreatCircleDistance().calculate, [None])
            self.assertRaises(ValueError, GreatCircleDistance().calculate, [""])

    def test_calculate__ValueError(self):
        self.assertRaises(ValueError, GreatCircleDistance().calculate, [None])
        self.assertRaises(ValueError, GreatCircleDistance().calculate, [""])
        data = [
            {
                'latitude': 0.0,
                'user_id': 69,
                'name': 'John Doe',
                'longitude': 0.0
            }
        ]
        self.assertRaises(ValueError, GreatCircleDistance().compute_distance, data)


class TestInvitationGenerator(unittest.TestCase):
    @patch("distance_calculator.core.write_output_to_file")
    def test_perform__get_invited_clients__Empty(self, wof):
        wof.json.dumps.return_value = "[]"
        wof.os.path.abspath.return_value = "~/intercom/../output.txt"
        wof.os.path.join.return_value = wof.os.path.abspath()
        wof.open.return_value = mock_open()
        with patch("distance_calculator.core.get_invited_clients", return_value=[]):
            self.assertEqual(InvitationGenerator().perform([]), [])

    def test_perform__get_invited_clients__ValueError(self):
        with patch("distance_calculator.core.get_invited_clients", side_effect=ValueError):
            self.assertRaises(ValueError, InvitationGenerator().perform, None)
            self.assertRaises(ValueError, InvitationGenerator().perform, "")

    @patch("distance_calculator.core.write_output_to_file")
    def test_perform__Empty(self, wof):
        wof.json.dumps.return_value = "[]"
        wof.os.path.abspath.return_value = "~/intercom/../output.txt"
        wof.os.path.join.return_value = wof.os.path.abspath()
        wof.open.return_value = mock_open()
        self.assertEqual(InvitationGenerator().perform([]), [])

    def test_perform__should_write_False__Empty(self):
        self.assertEqual(InvitationGenerator().perform([], False), [])

    def test_perform__ValueError(self):
        self.assertRaises(ValueError, InvitationGenerator().perform, None)
        self.assertRaises(ValueError, InvitationGenerator().perform, "")


class TestHelpers(unittest.TestCase):
    def test_convert_to_radians__ValueError(self):
        self.assertRaises(ValueError, convert_to_radians, None)
        self.assertRaises(ValueError, convert_to_radians, "")

    def test_get_A_B__ValueError(self):
        self.assertRaises(ValueError, get_A_B, None)
        self.assertRaises(ValueError, get_A_B, [])
        self.assertRaises(ValueError, get_A_B, {})
        self.assertRaises(ValueError, get_A_B, "")

    def test_compute_d_rho__ValueError(self):
        self.assertRaises(ValueError, compute_d_rho, None, T1, L2, T2)
        self.assertRaises(ValueError, compute_d_rho, None, T1, L2, None)
        self.assertRaises(ValueError, compute_d_rho, None, T1, None, T2)
        self.assertRaises(ValueError, compute_d_rho, None, T1, None, None)
        self.assertRaises(ValueError, compute_d_rho, None, None, L2, T2)
        self.assertRaises(ValueError, compute_d_rho, None, None, L2, None)
        self.assertRaises(ValueError, compute_d_rho, None, None, None, T2)
        self.assertRaises(ValueError, compute_d_rho, None, None, None, None)
        self.assertRaises(ValueError, compute_d_rho, L1, T1, L2, None)
        self.assertRaises(ValueError, compute_d_rho, L1, T1, None, T2)
        self.assertRaises(ValueError, compute_d_rho, L1, T1, None, None)
        self.assertRaises(ValueError, compute_d_rho, L1, None, L2, T2)
        self.assertRaises(ValueError, compute_d_rho, L1, None, L2, None)
        self.assertRaises(ValueError, compute_d_rho, L1, None, None, T2)
        self.assertRaises(ValueError, compute_d_rho, L1, None, None, None)

    def test_compute_d__ValueError(self):
        self.assertRaises(ValueError, compute_d, None)
        self.assertRaises(ValueError, compute_d, 0.0)
        self.assertRaises(ValueError, compute_d, 0)

    def test_get_invited_clients__ValueError(self):
        self.assertRaises(ValueError, get_invited_clients, None)
        self.assertRaises(ValueError, get_invited_clients, "")
        self.assertRaises(ValueError, get_invited_clients, {})


if __name__ == "__main__":
    unittest.main()
