import unittest

from unittest.mock import Mock, patch, mock_open

from .const import (PARSED_API_TEXT, DISTANCE_CALCULATED_TEXT,
                    INVITATION_LIST, DATA_DEGREE,
                    DATA_RADIAN, RADIAN_CONVERTED_TEXT,
                    L1, T1, L2, T2, DISTANCE, D_RHO,
                    OUTPUT_FILE_CONTENT)
from ..core import GreatCircleDistance, InvitationGenerator
from ..helpers import (convert_to_radians, get_A_B, compute_d_rho,
                       compute_d, get_invited_clients, write_output_to_file)


class TestGreatCircleDistance(unittest.TestCase):
    def test_compute_distance__success(self):
        cd = GreatCircleDistance().compute_distance(RADIAN_CONVERTED_TEXT)
        for i, distance in enumerate(DISTANCE_CALCULATED_TEXT):
            self.assertAlmostEqual(cd[i]["latitude"], distance["latitude"])
            self.assertAlmostEqual(cd[i]["longitude"], distance["longitude"])
            self.assertAlmostEqual(cd[i]["distance"], distance["distance"])

    def test_calculate__success(self):
        c = GreatCircleDistance().calculate(PARSED_API_TEXT)
        for i, distance in enumerate(DISTANCE_CALCULATED_TEXT):
            self.assertAlmostEqual(c[i]["latitude"], distance["latitude"])
            self.assertAlmostEqual(c[i]["longitude"], distance["longitude"])
            self.assertAlmostEqual(c[i]["distance"], distance["distance"])


class TestInvitationGenerator(unittest.TestCase):
    def test_perform__write_True__success(self):
        with patch("distance_calculator.core.write_output_to_file", return_value=None) as wf:
            self.assertEqual(InvitationGenerator().perform(DISTANCE_CALCULATED_TEXT), INVITATION_LIST)

    def test_perform__write_False__success(self):
        self.assertEqual(InvitationGenerator().perform(DISTANCE_CALCULATED_TEXT, False), INVITATION_LIST)


class TestHelpers(unittest.TestCase):
    def test_convert_to_radians__success(self):
        self.assertEqual(convert_to_radians(DATA_DEGREE), DATA_RADIAN)

    def test_get_A_B__success(self):
        expected_result = L1, T1, L2, T2
        self.assertEqual(get_A_B(DATA_RADIAN), expected_result)

    def test_compute_d_rho__success(self):
        self.assertAlmostEqual(compute_d_rho(L1, T1, L2, T2), D_RHO)

    def test_compute_d__success(self):
        self.assertEqual(compute_d(D_RHO), DISTANCE)

    def test_get_invited_clients__success(self):
        self.assertEqual(get_invited_clients(DISTANCE_CALCULATED_TEXT), INVITATION_LIST)

    @patch("distance_calculator.helpers.os")
    @patch("distance_calculator.helpers.json")
    def test_write_output_to_file__success(self, j, os):
        j.dumps.return_value = OUTPUT_FILE_CONTENT
        os.path.abspath.return_value = "~/intercom/../output.txt"
        os.path.join.return_value = os.path.abspath("")
        with patch("distance_calculator.helpers.open", mock_open()) as mf:
            write_output_to_file(INVITATION_LIST)
            mf.assert_called_once_with(os.path.join(), "w+")
            mf().write.assert_called_once_with(OUTPUT_FILE_CONTENT)


if __name__ == "__main__":
    unittest.main()
