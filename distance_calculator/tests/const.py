PARSED_API_TEXT = [
    {
        'latitude': 51.92893,
        'user_id': 1,
        'name': 'Alice Cahill',
        'longitude': -10.27699
    },
    {
        'latitude': 53.2451022,
        'user_id': 4,
        'name': 'Ian Kehoe',
        'longitude': -6.238335
    },
    {
        'latitude': 53.1302756,
        'user_id': 5,
        'name': 'Nora Dempsey',
        'longitude': -6.2397222
    }
]
RADIAN_CONVERTED_TEXT = [
    {
        'latitude': 0.906330805537659,
        'user_id': 1,
        'name': 'Alice Cahill',
        'longitude': -0.17936731269453204},
    {
        'latitude': 0.9293023439508763,
        'user_id': 4,
        'name': 'Ian Kehoe',
        'longitude': -0.10887948559240046},
    {
        'latitude': 0.9272982417120057,
        'user_id': 5,
        'name': 'Nora Dempsey',
        'longitude': -0.10890369679978412
    }
]
DISTANCE_CALCULATED_TEXT = [
    {
        'latitude': 0.906330805537659,
        'user_id': 1,
        'name': 'Alice Cahill',
        'longitude': -0.17936731269453204,
        'distance': 313.25563378141084},
    {
        'latitude': 0.9293023439508763,
        'user_id': 4,
        'name': 'Ian Kehoe',
        'longitude': -0.10887948559240046,
        'distance': 10.566936289363076},
    {
        'latitude': 0.9272982417120057,
        'user_id': 5,
        'name': 'Nora Dempsey',
        'longitude': -0.10890369679978412,
        'distance': 23.28732066320704
    }
]
INVITATION_LIST = [
    {'user_id': 4, 'name': 'Ian Kehoe'},
    {'user_id': 5, 'name': 'Nora Dempsey'}
 ]

OUTPUT_FILE_CONTENT = """[
    {
        "user_id": 4,
        "name": "Ian Kehoe"
    },
    {
        "user_id": 5,
        "name": "Nora Dempsey"
    }
]
"""

DATA_DEGREE = {
    'latitude': 52.986375,
    'user_id': 12,
    'name': 'Christina McArdle',
    'longitude': -6.043701
}

DATA_RADIAN = {
    'latitude': 0.9247867024464105,
    'user_id': 12,
    'name': 'Christina McArdle',
    'longitude': -0.10548248145607382
}

DUBLIN_HQ_LAT_RADIAN = 0.9309486397304539
DUBLIN_HQ_LONG_RADIAN = -0.10921684028351844

D_RHO = 0.00655607055421258
L1 = -0.10548248145607382
T1 = 0.9247867024464105
L2 = -0.10921684028351844
T2 = 0.9309486397304539

DISTANCE = 41.76872550088835

