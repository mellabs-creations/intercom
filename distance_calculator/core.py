from .helpers import (convert_to_radians, get_A_B, compute_d_rho,
                      compute_d, get_invited_clients, write_output_to_file)


class GreatCircleDistance(object):
    def __init__(self):
        """Great-Circle Distance Service.
        """
        pass

    def compute_distance(self, data, *args, **kwargs):
        """Computes the distance between two points.

        Args:
            data (list): List of Client Data including "name", "user_id", "latitude", and "longitude".
            args (list): Function Arguments.
            kwargs (dict): Function Arguments as key-value pair.

        Returns:
            list: List of Client Data with "distance" key stored great-circle computed distance.

        Notes:
            1. The distance computation is referred from 'https://en.wikipedia.org/wiki/Great-circle_distance'.

        """
        for i, client_data in enumerate(data):
            L1, T1, L2, T2 = get_A_B(client_data)
            d_rho = compute_d_rho(L1, T1, L2, T2)
            d = compute_d(d_rho)
            data[i]["distance"] = d
        return data

    def calculate(self, data, *args, **kwargs):
        """Performs calculations of distance between the co-ordinates of current client and Intercom Dublin HQ.

        Args:
            data (list): List of Client Data including "name", "user_id", "latitude", and "longitude".
            args (list): Function Arguments.
            kwargs (dict): Function Arguments as key-value pair.

        Returns:
            list: List of Client Data with "distance" key stored great-circle computed distance.

        Raises:
            ValueError

        Notes:
            1. If the API Contract is breached, this function will raise 'KeyError'.
            This is not being handled to fail gracefully by the current scope of this function.
            2. All the iterations are being gracefully handled to cater to IndexError.

        """
        return self.compute_distance(list(map(lambda x: convert_to_radians(x), data)))


class InvitationGenerator(object):
    def __init__(self):
        """Invitation Generator Service.
        """
        pass

    def perform(self, data, should_write=True, *args, **kwargs):
        """Performs Invitation Generation across the distances of Clients.

        Args:
            data (list): Client parsed API Data along with the distance from Intercom Dublin HQ in km.
            should_write (bool, optional): Flag to indicate writing of results to 'output.txt'. Defaults to True.
            args (list): Function Arguments.
            kwargs (dict): Function Arguments as key-value pair.

        Returns:
            list: output of the closest clients.

        """
        output = get_invited_clients(data)
        if should_write:
            write_output_to_file(output)
        return output

