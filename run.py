import data_parser.core as dp_core
import distance_calculator.core as dc_core


if __name__ == "__main__":
    request = dp_core.DataHandler().perform("api")
    data = dp_core.Parser().perform(request, "api")
    distanced_data = dc_core.GreatCircleDistance().calculate(data)
    dc_core.InvitationGenerator().perform(distanced_data)
