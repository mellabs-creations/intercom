#!/bin/bash
#!/usr/local/bin/python3
coverage run -m unittest discover
coverage report --omit=/usr/*,/Users/*,*/__init__.py,*/tests/*.py,*/const.py
coverage xml --omit=/usr/*,/Users/*,*/__init__.py,*/tests/*.py,*/const.py
